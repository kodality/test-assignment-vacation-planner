export interface VacationRequest {
  employee: string;
  startDate: Date;
  endDate: Date;
  comment: string;
  submittedAt: Date;
}
